#!/usr/bin/python
"""
Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following generators and filters are supported but
cannot be set by settings file:

&params;
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2021
# (C) William Avery, 2021
#
# Distributed under the terms of the MIT license.
#

import sys
import os
import traceback
from datetime import date
import pickle
import re
import requests
import json
import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import (
    ExistingPageBot,
)

import mwparserfromhell

import sparqlfilehandler
from authorityFilter import authorityFilter, authorityFilterException 

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}  # noqa: N816


class GBIFScan(
    # Refer pywikobot.bot for generic bot classes
    ExistingPageBot,
):

    def __init__(self, generator, **kwargs):
        self.available_options.update({
            'sparqlfile': None,
            'SPARQLPARAM': None
        })

        super(GBIFScan, self).__init__(**kwargs)

        self.generator = generator
        self.rundate = date.today().isoformat()
        self.GBIF_Cache = {}
        self.cacheFile = os.path.expanduser("~/.GBIF_cache.txt")
        try:
            with open(self.cacheFile, 'rb') as cf:
                self.GBIF_Cache = pickle.load(cf)
                #pywikibot.output('Cache: ' + str(self.GBIF_Cache))
        except Exception as e:
            pywikibot.output('Cache file: ' + str(e))
        with open("quickfile.txt", "w") as quickfile:
            quickfile.write('')
        self.authorityFilter = authorityFilter()
        self.authDict = {}

    def teardown(self):

        slist = list(self.authDict.items())
        slist.sort(key=lambda item: item[1], reverse=True)
        pywikibot.output(slist)

        with open(self.cacheFile, 'wb') as cf:
            pickle.dump(self.GBIF_Cache, cf)

    def write_output_line(self, line):
        with open("quickfile.txt", "a") as quickfile:
            quickfile.write(line)

    def treat_page(self) -> None:

        authcount = {}
        pywikibot.output(self.current_page.title())
        p_item = self.current_page
        p_item_cont = None
        item_name = ''
        item_year = ''
        if not 'P846' in p_item.claims:
            pywikibot.output("No GBIF ID")
            return
        gbifId = p_item.claims.get('P846')[0].getTarget()
        if 'P225' in p_item.claims:
            name_item = p_item.claims.get('P225')[0]
            if 'P405' in name_item.qualifiers:
                return
            item_name = str(name_item.getTarget())
            pywikibot.output('"' + item_name + '"')
            if 'P574' in name_item.qualifiers:
                item_year = name_item.qualifiers.get('P574')[0].target
                pywikibot.output(f"Item year = {item_year}")
        auth = []
        year = None
        page = None
        missing_auth = []
        jResults = None
        pywikibot.output(item_name + "=GBIF: " + gbifId)
        if gbifId in self.GBIF_Cache:
            # pywikibot.output(self.GBIF_Cache[gbifId])
            jResults = json.loads(self.GBIF_Cache[gbifId])
            pywikibot.output('cache used')
        else:
            with requests.get(url=f"https://api.gbif.org/v1/species/{gbifId}") as resp:
                pywikibot.output(resp.status_code)
                if (resp.status_code == 404):
                    pywikibot.warn(f"GBIF 404 on {p_item.title()}")

                if (resp.status_code != 200):
                    raise Exception("API responded " +
                                    str(resp.status_code))

                # pywikibot.output(resp.content)
                jResults = json.loads(resp.content)
                if not 'authorship' in jResults:
                    raise Exception("API response had no author")
                rank = None
                if 'rank' in jResults:
                    rank = jResults['rank']
                canonicalName = None
                if 'canonicalName' in jResults:
                    canonicalName = jResults['canonicalName']
                pywikibot.output(
                    rank + ' ' + canonicalName)
                if canonicalName != item_name:
                    pywikibot.output(
                        jResults['canonicalName'] + "!=" + item_name)
                    return
                self.GBIF_Cache[gbifId] = resp.content
        gbif_auth = jResults['authorship'].strip()
        pywikibot.output(gbif_auth)
        rgx = re.compile(r"(\(?)\D+(\d\d\d\d)?(\)?$)")
        authmatch = rgx.match(gbif_auth)

        if not authmatch:
            print(jResults)
            return
        print(authmatch.groups())
        print(authmatch.group(1))
        recomb = bool(authmatch.group(1) == '(')
        print(f"recomb={recomb}")
        year = authmatch.group(2) if authmatch.group(2) else ''
        print(year)
        auths = gbif_auth.replace(year, '').replace('&', ',').strip()
        if recomb:
            auths = auths[1:-1].strip()
        if auths[-1] == ',':
            auths = auths[:-1].strip()
        authList = auths.split(",")
        print(authList)
        if auths:
            QIDList = []
            quickline = f'{p_item.getID()}\tP225\t"{item_name}"'
            for auth in authList:
                auth = auth.strip()
                try:
                    QIDs = self.authorityFilter.getQIDs(
                        auth, jResults, year if year else -1)

                    if len(QIDs) == 1:
                        QIDList.append(QIDs[0])
                        quickline += f"\tP405\t{QIDs[0]}"
                    else:
                        if not auth in self.authDict:
                            self.authDict[auth] = 1
                        else:
                            self.authDict[auth] += 1
                except authorityFilterException as e:
                    print(e)
            if year:
                if not item_year or item_year != year:
                    quickline += f'\tP574\t+{year}-00-00T00:00:00Z'
                if item_year and item_year != year:
                    pywikibot.output(
                        f"Year mismatch on {item_name}: source {year} <> existing {item_year}")
            if 'issues' in jResults and "PUBLISHED_BEFORE_GENUS" in jResults["issues"]:
                pywikibot.output(f"{item_name}: PUBLISHED_BEFORE_GENUS")
                return
            if 'deleted' in jResults and jResults["deleted"]:
                pywikibot.output(f"{item_name}: Deleted")
                return
            if len(QIDList) == len(authList):
                if recomb:
                    quickline += f'\tP3831\tQ14594740'
                quickline += f'\tS248\tQ1531570\tS813\t+{self.rundate}T00:00:00Z/11\n'
                self.write_output_line(quickline)
            else:
                if len(authList) > 1:
                    if not auths in self.authDict:
                        self.authDict[auths] = 1
                    else:
                        self.authDict[auths] += 1


def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine on which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # generate -sparql parameter from sparqlfilehandler parameters
    local_args = sparqlfilehandler.handle_params(local_args, echo=True)

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        # pass generator and private options to the bot
        bot = GBIFScan(generator=gen, **options)
        bot.run()  # guess what it does
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
