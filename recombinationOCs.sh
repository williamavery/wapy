export QID=$1
export TOOL=william-avery-bot
export TOOLHOME=/data/project/$TOOL
export QIDLC=${QID,,}
export NAME=recombination-ocs-$QIDLC
ssh williamavery@login.toolforge.org << EOF
become $TOOL
kubectl replace --force --validate=true -f - <<END
 apiVersion: batch/v1
 kind: Job
 metadata:
   name: $NAME
   labels:
     name: $NAME
     toolforge: $TOOL
     qid: $QID
 spec:
   template:
     metadata:
       labels:
         name: $NAME
         qid: $QID
         toolforge: tool # Required. Causes the shares to be mounted.
     spec:
       containers:
         - name: $NAME
           image: docker-registry.tools.wmflabs.org/toolforge-python39-sssd-base:latest
           command: [ "python3",
                      "$TOOLHOME/wikipythonics/recombinationScan.py",
                      "-family:wikidata", "-lang:wikidata",
                      "-sparqlfile:$TOOLHOME/wikipythonics/oc_not_set.sparql",
                      "-SPARQLPARAM:$QID",
                      "-outputfile:quickstat_oc_$QID.txt"]
           workingDir: $TOOLHOME
           env:
             - name: PYTHONPATH
               value: /data/project/shared/pywikibot/stable:$TOOLHOME/env39/lib/python3.9/site-packages
             - name: HOME
               value: $TOOLHOME
           imagePullPolicy: Always
       restartPolicy: Never
   backoffLimit: 0 # i.e. never really means never
END
EOF
