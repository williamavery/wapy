#!/usr/bin/python
"""
Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following generators and filters are supported but
cannot be set by settings file:

&params;
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2021
# (C) William Avery, 2021
#
# Distributed under the terms of the MIT license.
#

import sys
import os
import traceback
from datetime import date
import pickle
import re
import requests
import json

import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot

import mwparserfromhell

import sparqlfilehandler
from authorityFilter import authorityFilter, shortTaxa as st, dump_authority_file

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}  # noqa: N816


class authHarvester(
    ExistingPageBot
):

    def __init__(self, generator, **kwargs):
        self.available_options.update({
            'taxonkey': False
        })
        super().__init__(**kwargs)
        pywikibot.output(self.opt.taxonkey)
        self.generator = generator
        self.rundate = date.today().isoformat()
        self.GBIF_Cache = {}
        self.cacheFile = os.path.expanduser("~/.GBIF_cache.txt")
        self.taxonval = getattr(st, self.opt.taxonkey)
        try:
            with open(self.cacheFile, 'rb') as cf:
                self.GBIF_Cache = pickle.load(cf)
                #pywikibot.output('Cache: ' + str(self.GBIF_Cache))
        except Exception as e:
            pywikibot.output('Cache file: ' + str(e))
        with open("quickfile.txt", "w") as quickfile:
            quickfile.write('')
        self.authorityFilter = authorityFilter()
        self.authDict = {}

    def teardown(self):
        dump_authority_file(self.authorityFilter.authorityArray, 'newauths.csv') 
        with open(self.cacheFile, 'wb') as cf:
            pickle.dump(self.GBIF_Cache, cf)


    def treat_page(self) -> None:
        curAuth=None
        possibilities = set()
        family_name = None
        family_name_label = None
        family_name_key = None
        author_citation = None
        authorship=None
        p_item = self.current_page
        pywikibot.output(p_item.title())

        if 'en' in p_item.labels:
            pywikibot.output(p_item.labels.get('en'))
        p_item_cont = None
        item_name = ''
        item_year = ''
        if str(self.current_page.title()) in self.authorityFilter.authorityArray:
            curAuth = self.authorityFilter.authorityArray[p_item.title()]
            # pywikibot.output(curAuth)
            if self.taxonval in curAuth['taxa']:
                return
        if 'P734' in p_item.claims:
            family_name_key = p_item.claims.get('P734')[0].getTarget()
            if 'en' in family_name_key.labels:
                family_name_label = family_name_key.labels['en']
            family_name = family_name_key.claims.get(
                'P1705')[0].getTarget().text

        if 'P835' in p_item.claims:
            author_citation = p_item.claims.get('P835')[0].getTarget()

        pywikibot.output(f'family name = {family_name}')
        pywikibot.output(f'family name label = {family_name_label}')
        pywikibot.output(f'author_citation = {author_citation}')
        gbif_auth=None
        if family_name: possibilities.add(family_name)
        if family_name_label: possibilities.add(family_name_label)
        if author_citation: possibilities.add(author_citation)
        gbif_possibilities = set()
        for result in pywikibot.data.sparql.SparqlQuery().select(query=f"""SELECT ?item ?gbif_id WHERE {{
                ?item  p:P225 ?name.  
                ?name  pq:P405 wd:{p_item.title()}.
                ?item  p:P846 ?gbif.
                ?gbif  ps:P846 ?gbif_id.
                }} LIMIT 10"""
                                                                 ):
            it = str(result['item'])
            s = it.split('/')
            s.reverse()
            qid = s[0] if s else None
            #pywikibot.output(result['gbif_id'])
            gbifId = result['gbif_id']

            if gbifId in self.GBIF_Cache:
                # pywikibot.output(self.GBIF_Cache[gbifId])
                jResults = json.loads(self.GBIF_Cache[gbifId])
                pywikibot.output('cache used')
            else:
                with requests.get(url=f"https://api.gbif.org/v1/species/{gbifId}") as resp:
                    pywikibot.output(resp.status_code)
                    if (resp.status_code != 200):
                        raise Exception("API responded " +
                                        str(resp.status_code))

                    jResults = json.loads(resp.content)
                    if not 'authorship' in jResults:
                        raise Exception("API response had no author")
                    if not 'kingdom' in jResults or jResults['kingdom'] != 'Animalia':
                        pywikibot.output(jResults['kingdom'])
                        continue
            intm = str(jResults['authorship']).replace('(', '').replace('&',',')
            intm=re.sub(r",\s*\d\d\d\d\)?", '', intm)
            authorships = intm.split(',')
            for auth in authorships:
                if auth := auth.strip():
                    if auth in possibilities:
                        authorship=auth
                        break
                    else:
                        gbif_possibilities.add(auth)
                        pywikibot.output(qid)
                        pywikibot.output(gbifId)
                        pywikibot.output(intm)
            if authorship: # GBIF is using one of the values already found
                break      # ... so look no further
        pywikibot.output(f"GBIF = {authorship}")
        pywikibot.output(f"GBIF poss. = {gbif_possibilities}")

        if not authorship: possibilities |= gbif_possibilities
        possibilities = list(possibilities)
        pywikibot.output(possibilities)
        ans=[]
        sc=[]
        rep=None
        if not curAuth: curAuth = {"abbrList": [], 'taxa': []}
        else: curAuth['abbrList']=curAuth['abbrList'].split('|')
        pywikibot.output(curAuth)
        if self.taxonval not in curAuth['taxa']:
            curAuth['taxa'].append(self.taxonval)
        while rep not in ('q', 's'):
            ans=[]
            for i, poss in enumerate(possibilities):
                i+=1

                if poss not in curAuth['abbrList']:
                    ans.append((poss, str(i)))
                    print(f"{i}\t{poss}")
            if curAuth['abbrList']:
                ans.append(('Save', 's'))
            ans.append(('Quit', 'q'))

            rep = pywikibot.input_choice(u"Action", ans, default='q', automatic_quit=False)
            if not rep in ('s', 'q'):
                pywikibot.output(rep)
                curAuth['abbrList'].append(possibilities[int(rep)-1])
                pywikibot.output(curAuth)
        if rep=='s':
            #curAuth['abbrList']='|'.join(curAuth['abbrList'])
            curAuth['abbrList']='|'.join(curAuth['abbrList'])
            self.authorityFilter.authorityArray[p_item.title()]=curAuth
        



def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine on which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # generate -sparql parameter from sparqlfilehandler parameters
    local_args = sparqlfilehandler.handle_params(local_args, echo=True)

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    for arg in local_args:

        option, sep, value = arg.partition(':')
        if option == "-taxonkey":
            options['taxonkey'] = value
            continue
    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        # pass generator and private options to the bot
        bot = authHarvester(generator=gen, **options)
        bot.run()
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
