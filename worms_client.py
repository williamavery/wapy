from zeep import Client

client = Client('https://www.marinespecies.org/aphia.php?p=soap&wsdl=1')
#service=client.create_service(
#    "{http://itis_service.itis.usgs.gov}ITISServiceSoap12Binding",
#    "https://www.itis.gov/ITISWebService/services/ITISService.ITISServiceHttpsSoap12Endpoint/")
id=client.service.getAphiaID(scientificname="Murex olearium", marine_only=False)
print(type(id))
print(id)
rec=client.service.getAphiaRecordByID(id)
print(type(rec))
print(rec)