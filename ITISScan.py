#!/usr/bin/python
"""
Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following generators and filters are supported but
cannot be set by settings file:

&params;
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2021
# (C) William Avery, 2021
#
# Distributed under the terms of the MIT license.
#

import sys
import os
from datetime import date
from time import sleep
import requests

from zeep import Client, Transport

import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import (
    ExistingPageBot,
    SingleSiteBot
)

import sparqlfilehandler

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}  # noqa: N816


class ITISScan(
    ExistingPageBot,
    SingleSiteBot
):
    @staticmethod
    def runqry(itis_tsn):
        squery= f"""SELECT ?item 
        WHERE
        {{
            ?item p:P815 ?statement0.
            ?statement0 (ps:P815) "{itis_tsn}".
        }}"""
        qry_reslt = pywikibot.data.sparql.SparqlQuery().select(query=squery)
        if not len(qry_reslt):
            return None
        s = str(qry_reslt[0]['item']).split('/')
        return s[len(s)-1] if s else None

    def __init__(self, generator, **kwargs):
        self.available_options.update({
            'sparqlfile': None,
            'SPARQLPARAM': None
        })

        super(ITISScan, self).__init__(**kwargs)

        self.generator = generator

        self.quickfilename='./ITISquickfile.txt'
        with open(self.quickfilename, "w") as quickfile:
            quickfile.write('')
                
        client = Client('https://www.itis.gov/ITISWebService/services/ITISService?wsdl',
                        transport=Transport(timeout=60, operation_timeout=60))
        self.ITIS=client.create_service(
            "{http://itis_service.itis.usgs.gov}ITISServiceSoap12Binding",
            "https://www.itis.gov/ITISWebService/services/ITISService.ITISServiceHttpsSoap12Endpoint/")

    def teardown(self):
            pass

    def write_output_line(self, line):
        with open(self.quickfilename, "a") as quickfile:
            quickfile.write(line)

    def queryITIS(self, func, itisTSN):
        response=None
        retry_limit=100
        retries = 0
        while retries < retry_limit:
            try:
                response=self.ITIS[func](tsn=itisTSN)
                break
            except (requests.exceptions.RequestException) as exc:
                pywikibot.output(repr(exc))
                retries+=1
                interval=min([retries * 2, 60])
                pywikibot.output(f"Retrying ITIS in {interval}s. Retry {retries}")
                sleep(interval)
        return response

    def treat_page(self) -> None:

        p_item = self.current_page
        item_name = ''

        if not 'P815' in p_item.claims:
            pywikibot.output("No ITIS TSN")
            return
        itisTSN = p_item.claims.get('P815')[0].getTarget()
        if 'P225' in p_item.claims:
            name_item = p_item.claims.get('P225')[0]
            item_name = str(name_item.getTarget())

        acceptedName = ''
        canonicalName = ''

        pywikibot.output(item_name + "=ITIS: " + itisTSN)

        coreMetadata = self.queryITIS('getCoreMetadataFromTSN', itisTSN)

        if not coreMetadata :
            pywikibot.output("coreMetadata response was falsy")
            return

        if 'unacceptReason' in coreMetadata and coreMetadata['unacceptReason']=='original name/combination':
            pywikibot.output('PROTONYM')
            pywikibot.output(coreMetadata['unacceptReason'])
        else:
            return

        acceptedNames = self.queryITIS('getAcceptedNamesFromTSN', itisTSN)

        if not acceptedNames :
            pywikibot.output("acceptedNames response was falsy")
            return

        if 'acceptedNames' in acceptedNames:
            acceptedName = acceptedNames['acceptedNames'][0]['acceptedName']

        pywikibot.output('Accepted name=' + acceptedName)
        scientificName = self.queryITIS('getScientificNameFromTSN', itisTSN)

        if not scientificName :
            pywikibot.output("scientificName response was falsy")
            return

        if 'combinedName' in scientificName:
            canonicalName=scientificName['combinedName']

        if canonicalName != item_name:
            pywikibot.output(canonicalName + "!=" + item_name)
            return

        recombqid = ITISScan.runqry(acceptedNames['acceptedNames'][0]['acceptedTsn'])
        if not recombqid: 
            pywikibot.output(f"failed to find QID for ITIS TSN {acceptedNames['acceptedNames'][0]['acceptedTsn']}")
            return

        names = p_item.claims.get('P225')
        if len(names) != 1:
            pywikibot.output(f'Expected one taxon name on {qid}, found {len(names)}')
            return
        tname=names[0].getTarget()
        if tname == acceptedName:
            pywikibot.output(f"Expected match for {acceptedName} on {qid}, found {tname}")
            return

        recombpg=pywikibot.ItemPage(self.site, recombqid)
        qid=p_item.getID()
        ref=f'\tS248\tQ82575\tS813\t+{date.today().isoformat()}T00:00:00Z/11\n'

        generate_protonym=True
        generate_oc=True

        if 'P2868' in p_item.claims:
            for protonym in [r for r in p_item.claims.get('P2868') if r.target_equals('Q14192851')]:
                if protonym.has_qualifier('P642', recombqid):
                    generate_protonym=False
                    continue

        if generate_protonym:
            self.write_output_line(f'{qid}\tP2868\tQ14192851\tP642\t{recombqid}' + ref)

        if 'P1403' in recombpg.claims:
            if [r for r in recombpg.claims.get('P1403') if r.target_equals(qid)]:
                    generate_oc=False

        if generate_oc:
            self.write_output_line(f'{recombqid}\tP1403\t{qid}' + ref)

def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine on which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # generate -sparql parameter from sparqlfilehandler parameters
    local_args = sparqlfilehandler.handle_params(local_args, echo=True)

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        # pass generator and private options to the bot
        bot = ITISScan(generator=gen, **options)
        bot.run()  # guess what it does
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
