#
# (C) William Avery, 2021
#
# Distributed under the terms of the MIT license.
#
import csv
import re

import pywikibot
import pywikibot.data.sparql as sparql


class authorityFilterException(Exception):
    def __init__(self, errorArguments):
        super().__init__(f"Authority Filter: {errorArguments}")

class authFileWarning(UserWarning):
    def __init__(self, errorArguments):
        super().__init__(f"Loading file of authoritites: {errorArguments}")

class shortTaxa():
    arthropoda = ["phylum", "Arthropoda"]
    lepidoptera = ["order", "Lepidoptera"]
    coleoptera = ["order", "Coleoptera"]

    mollusca = ["phylum", "Mollusca"]

    chordata = ["phylum", "Chordata"]

    def __init__(self):
        pass
    def encode(self, taxa):
        retarr=[]
        for taxon in taxa:
            valfound = False
            for var, val in shortTaxa.__dict__.items():
                if var[:1] != '_' and val==taxon:
                    retarr.append(var)
                    valfound = True
                    break
            if not valfound:
                retarr.append(f"{taxon[0]}={taxon[1]}")
        return ','.join(retarr)

    def decode(self, taxastring):
        taxa=taxastring.split(',')
        retarr=[]
        for taxon in taxa:
            if hasattr(shortTaxa, taxon):
                retarr.append(getattr(shortTaxa, taxon))
            else:
                t = '='.split(taxon)
                if len(t)==2:
                    retarr.append([t[0], t[1]])
                else:
                    raise authorityFilterException(f'could not decode "{taxon}"')
        return retarr

def load_authority_file(csvfilename, encoding='UTF-8'):
    def setintfield(field, authObj, fldkey, linenum):
        try:
            if (f := field.strip()):
                authObj[fldkey] = int(f)
        except Exception as e:
            raise authorityFilterException(
                f"Error setting {fldkey} from value '{field}' at line {linenum} of {csvfilename}") from e
        return authObj

    authorityArray = {}
    qid_regx = re.compile(r"Q\d+")
    with open(csvfilename, 'r', encoding=encoding) as file:
        reader = csv.reader(file)
        for field in reader:
            if qid_regx.match(field[0]):
                if field[0] in authorityArray:
                    raise authorityFilterException(
                        f'Duplicate QID when loading authors :{field[0]}')
                authObj = {"abbrList": field[1]}
                authObj = setintfield(field[2], authObj, 'startYear', reader.line_num)
                authObj = setintfield(field[3], authObj, 'endYear', reader.line_num)
                authObj["taxa"] = []
                authObj["taxa"]=shortTaxa().decode(field[4])
                #print(authObj)
                authorityArray[field[0]]=authObj
            else:
                pywikibot.warn(authFileWarning(f"Line {reader.line_num} of {csvfilename} does not start with a QID and has been ignored"))
    return authorityArray

def dump_authority_file(authorityArray, csvfilename, encoding='UTF-8'):
    with open(csvfilename, 'w', encoding=encoding) as file:
        writer = csv.writer(file)
        for qid, authObj in authorityArray.items():
            abbrlist='|'.join(authObj['abbrList'])
            writer.writerow(
                (   
                    qid, 
                    authObj['abbrList'], 
                    authObj['startYear'] if 'startYear' in authObj else None,
                    authObj['endYear'] if 'endYear' in authObj else None,
                    shortTaxa().encode(authObj['taxa']) if 'taxa' in authObj else None
                )
            )

class authorityFilter ():

    def __init__(self):
        st = shortTaxa()
        self.authorityArray = {
# Friese": {"qid": "Q91144", "taxa": {"order": "Hymenoptera"}},
# Gibbs": {"qid": "Q22113142", "taxa": {"family": "Micropterigidae"}},
# Giordani Soika": {"qid": "Q21503073", "taxa": {"order": "Hymenoptera"}},
# Fluke": {"qid": "Q55033459", "taxa": {"order": "Diptera"}},
    # Forel": {"qid": "Q22530", "taxa": {"order": "Hymenoptera"}},
    # Förster": {"qid": "Q895467", "taxa": {"order": "Hymenoptera"}},

# Griveaud": {"qid": "Q21339892", "taxa": {"class": "Insecta"}},
# Grote": {"qid": "Q770604", "taxa": {"order": "Lepidoptera"}},
# Günther": {"qid": "Q57514", "taxa": {"phylum": "Chordata"}},
# Hampson": {"enlink": "George Hampson", "taxa": {"class": "Insecta"}},
# Harvey": {"qid": "Q83276385", "taxa": {"order": "Lepidoptera"}},
# Hashimoto": {"qid": "Q22112568", "taxa": {"family": "Micropterigidae"}},
# Helfer": {"qid": "Q38077867", "taxa": {"family": "Saturniidae"}}
# Hood": {"qid": "Q22107945", "taxa": {"order": "Thysanoptera"}},,
# Hutton": {"qid": "Q108802437", "taxa": {"family": "Saturniidae"}},
# Jerdon": {"qid": "Q1335271", "taxa": {"phylum": "Chordata"}},
# Karg: {"qid": "Q21502558", "taxa": {"class": "Arachnida"}},
# Kailola": {"qid": "Q18338271", "taxa": {"phylum": "Chordata"}},

# Kottelat": {"enlink": "Maurice Kottelat"},
# Kumata": {"qid": "Q21338652", "taxa": {"family": "Gracillariidae"}},
# "Kuznetzov": {"qid": "", "taxa": {"family": "Gracillariidae"}}, V. I. Kuznetzov

# Latreille": {"enlink": "Pierre André Latreille"},Hugo Hendrikus
# Lemaire": {"qid": "Q484852", "taxa": {"order": "Lepidoptera"}},
# Matsumura": {"qid": "Q11104508", "taxa": {"class": "Insecta"}},
# McDunnough": {"enlink": "James Halliday McDunnough"},
# Mell": {"qid": "Q55313519", "taxa": {"order": "Lepidoptera"}},

# Meyrick": {"enlink": "Edward Meyrick", "taxa": {"class": "Insecta"}},
# Mitchell": {"qid": "Q18155792", "taxa": {"order": "Hymenoptera"}},
# Nalepa": {"qid": "Q15145575", "taxa": {"class": "Arachnida"}},
# Motschulsky": {"qid": "Q738769", "taxa": {"order": "Coleoptera"}},
# Neumoegen": {"qid": "Q21341124", "taxa": {"order": "Lepidoptera"}},
# Oiticica": {"qid": "Q22114000", "taxa": {"order": "Lepidoptera"}},
# Pasteels": {"qid": "Q25796416", "taxa": {"order": "Hymenoptera"}},
# Ragonot": {"qid": "Q2347805", "taxa": {"order": "Lepidoptera"}},
# Pinhey": {"enlink": "Elliot Pinhey", "taxa": {"class": "Insecta"}},
# Röber": {"enlink": "Julius Röber", "taxa": {"order": "Lepidoptera"}},
# Rothschild": {"qid": "Q316669", "taxa": {"order": "Lepidoptera"}},
# Rougeot": {"qid": "Q21393170", "taxa": {"order": "Lepidoptera"}},
# Schaus": {"qid": "Q3568935", "taxa": {"order": "Lepidoptera"}},
# Shiraki": {"qid": "Q1445422", "taxa": {"order": "Diptera"}},
# Staudinger": {"qid": "Q67331", "taxa": {"order": "Lepidoptera"}},
# Strand": {"qid": "Q2537286", "taxa": {"phylum": "Arthropoda"}},
# Swezey": {"qid": "Q2039146", "taxa": {"order": "Lepidoptera"}},
# Tams": {"qid": "Q21338560", "taxa": {"order": "Lepidoptera"}},
# Tinkham": {"qid": "Q98515938", "taxa": {"class": "Insecta"}},
# Vachal": {"qid": "Q16026576", "taxa": {"order": "Hymenoptera"}},
# Valenciennes": {"enlink": "Achille Valenciennes"},
# Weymer": {"qid": "Q3121078", "taxa": {"order": "Lepidoptera"}},
        }
        self.site= pywikibot.Site('wikidata', 'wikidata')
        self.authorityArray=load_authority_file('auths.csv')

        self.authorityIndex= {}
        for auth in self.authorityArray:
            for abbr in self.authorityArray[auth]["abbrList"].split('|'):
                if abbr not in self.authorityIndex:
                    self.authorityIndex[abbr]= []
                self.authorityIndex[abbr].append(auth)

    def runMinQuery(self, qid):
        squery= f"""SELECT (YEAR(MIN(?year)) as ?startYear)  (YEAR(MAX(?year)) as ?endYear)
        WHERE
        {{
            ?item  p:P225 ?name.
            ?name  pq:P405 wd:{qid}.
            ?name  pq:P405 ?auth.
            ?name  pq:P574 ?year.
        }}"""
        qry= pywikibot.data.sparql.SparqlQuery().select(query=squery)
        pywikibot.output('minmax:' + str(qry))
        if not len(qry):
            return None
        return qry[0]

    def setAuthDets(self, auth):
        authPage= pywikibot.ItemPage(self.site, auth)
        claims = authPage.claims
        startYear= None
        authentry = self.authorityArray[auth]
        if 'P569' in authPage.claims and 'startYear' not in authentry:
            dob= claims.get('P569')[0].getTarget()
            authentry['startYear']= dob.year + 20

        if 'P570' in claims and 'endYear' not in authentry:
            dob= claims.get('P570')[0].getTarget()
            authentry['endYear']= dob.year + 3

        if 'startYear' not in authentry or 'endYear' not in authentry:
            reslt= self.runMinQuery(auth)
        
        if 'startYear' not in authentry:
            authentry['startYear']= 0
            if reslt and reslt['startYear']:
                authentry['startYear']= int(reslt['startYear'])
        if 'endYear' not in authentry:
            authentry['endYear'] = authentry['startYear'] + \
                100 if authentry['startYear'] else 9999
            if reslt and reslt['endYear']:
                authentry['endYear']= int(reslt['endYear'])

        authentry['set']= True

    def getQIDs(self, abbr, taxonHier, year):
        retAuthList= []
        if abbr not in self.authorityIndex:
            return retAuthList
        for auth in self.authorityIndex[abbr]:
            taxonMatch = False
            for taxonItem in self.authorityArray[auth]['taxa']:
                try:
                    if taxonHier[taxonItem[0]] == taxonItem[1]:
                        taxonMatch = True
                except Exception as exc:
                    raise authorityFilterException(
                        f"Taxon rank '{taxonItem[0]}' not found in taxonHier {str(taxonHier)}") from exc
            if taxonMatch:
                yearMatch = True
                if (('startYear' not in self.authorityArray[auth]
                     or 'endYear' not in self.authorityArray[auth])
                        and 'set' not in self.authorityArray[auth]):
                    self.setAuthDets(auth)
                intYear= int(year)
                if 'startYear' not in self.authorityArray[auth] or (intYear < self.authorityArray[auth]['startYear']):
                    yearMatch = False
                if 'endYear' not in self.authorityArray[auth] or (intYear > self.authorityArray[auth]['endYear']):
                    yearMatch = False
                if yearMatch:
                    retAuthList.append(auth)
        return retAuthList
