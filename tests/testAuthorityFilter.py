import unittest

from authorityFilter import *
class testAuthorityFilter(unittest.TestCase):
    def testEncode(self):
        self.assertEqual(shortTaxa().encode([['phylum', 'Mollusca']]), 'mollusca')
    def testDecode(self):
        self.assertEqual(shortTaxa().decode('mollusca'), [['phylum', 'Mollusca']])
    def testBadEndYear(self):
        with self.assertRaises(authorityFilterException) as afe:
            authdict=load_authority_file("tests/badyear.csv")
            
    def testAmsel(self):
        qid = 'Q1491291'
        leps = {'order': 'Lepidoptera'}
        testAF = authorityFilter()
        Amsel = testAF.setAuthDets(qid)
        self.assertTrue(testAF.authorityArray[qid]['set'])
        self.assertEqual(testAF.authorityArray[qid]['startYear'], 1925)
        self.assertEqual(testAF.authorityArray[qid]['endYear'], 2002)
        self.assertEqual(testAF.getQIDs('Amsel', leps, '1925'), [qid])
        self.assertEqual(testAF.getQIDs('Amsel', leps, '1924'), [])
        self.assertEqual(testAF.getQIDs('Amsel', leps, '2002'), [qid])
        self.assertEqual(testAF.getQIDs('Amsel', leps, '2003'), [])

        self.assertEqual(testAF.getQIDs('Amsel', {'order': 'Carnivora'}, '2002'), [])
        with self.assertRaises(authorityFilterException) as afe:
            testAF.getQIDs('Amsel', {}, '2002'), [qid]
        self.assertEqual(str(afe.exception), "Authority Filter: Taxon rank \'order\' not found in taxonHier {}")
        

    def testAurivillius(self):
        qid = 'Q1086510'
        auth = 'Aurivillius'
        testtaxo = {'order': 'Coleoptera'}
        testAF = authorityFilter()
        result = testAF.setAuthDets(qid)
        print(testAF.authorityIndex)
        print(testAF.authorityArray[qid])
        self.assertTrue(testAF.authorityArray[qid]['set'])
        self.assertEqual(testAF.authorityArray[qid]['startYear'], 1863)
        self.assertEqual(testAF.authorityArray[qid]['endYear'], 1931)
        self.assertEqual(testAF.getQIDs(auth, testtaxo, 1864), [qid])
        self.assertEqual(testAF.getQIDs(auth, testtaxo, 1862), [])
        self.assertEqual(testAF.getQIDs(auth, testtaxo, 1930), [qid])
        self.assertEqual(testAF.getQIDs(auth, testtaxo, 1933), [])

        self.assertEqual(testAF.getQIDs(auth, {'order': 'Decapoda'}, '1922'), [])
        with self.assertRaises(authorityFilterException) as afe:
            testAF.getQIDs(auth, {}, 1900), [qid]
        self.assertEqual(str(afe.exception), "Authority Filter: Taxon rank 'order' not found in taxonHier {}")

    def testBouyer(self):
        qid = 'Q21341706'
        auth = 'Bouyer'
        testtaxo = {'order': 'Lepidoptera'}
        testAF = authorityFilter()
        result = testAF.setAuthDets(qid)
        self.assertTrue(testAF.authorityArray[qid]['set'])
        self.assertEqual(testAF.authorityArray[qid]['startYear'], 1988)
        self.assertEqual(testAF.authorityArray[qid]['endYear'], 2013)
        self.assertEqual(testAF.getQIDs(auth, testtaxo, 1988), [qid])
        self.assertEqual(testAF.getQIDs(auth, testtaxo, 1887), [])
        self.assertEqual(testAF.getQIDs(auth, testtaxo, 2013), [qid])
        self.assertEqual(testAF.getQIDs(auth, testtaxo, 2014), [])

        self.assertEqual(testAF.getQIDs(auth, {'order': 'Decapoda'}, '2000'), [])
        with self.assertRaises(authorityFilterException) as afe:
            testAF.getQIDs(auth, {'suborder': ''}, 1900), [qid]
        self.assertEqual(str(afe.exception), "Authority Filter: Taxon rank 'order' not found in taxonHier {'suborder': ''}")

if __name__ == '__main__':
    unittest.main()