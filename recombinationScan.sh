export TOOL=william-avery-bot
export TOOLHOME=/data/project/$TOOL
function outputpods()
{
for QID in "${@:1}"; do
export PODNAME=recombination-scan-${QID,,}
cat <<$$$$
become $TOOL
kubectl replace --force --validate=true -f - <<END
 apiVersion: batch/v1
 kind: Job
 metadata:
   name: $PODNAME
   labels:
     name: $PODNAME
     qid: $QID
     toolforge: $TOOL
 spec:
   template:
     metadata:
       labels:
         name: $PODNAME
         qid: $QID
         toolforge: tool # Required. Causes the shares to be mounted.
     spec:
       containers:
         - name: $PODNAME
           image: docker-registry.tools.wmflabs.org/toolforge-python39-sssd-base:latest
           command: [ "python3",
                      "$TOOLHOME/wikipythonics/recombinationScan.py",
                      "-family:wikidata", "-lang:wikidata",
                      "-sparqlfile:$TOOLHOME/wikipythonics/recombSetter.sparql",
                      "-SPARQLPARAM:$QID",
                      "-outputfile:quickstat_$QID.txt"]
           workingDir: $TOOLHOME
           env:
             - name: PYTHONPATH
               value: /data/project/shared/pywikibot/stable:$TOOLHOME/env39/lib/python3.9/site-packages
             - name: HOME
               value: $TOOLHOME
           imagePullPolicy: Always
       restartPolicy: Never
   backoffLimit: 0 # i.e. never really means never
END
$$$$
done
}

function outputjob()
{
outputpods ${@:1 }
}

outputjob ${@:1} | ssh williamavery@login.toolforge.org
