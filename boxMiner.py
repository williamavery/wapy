#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
"""
#
# (C) William Avery 2021, based on basic.py
# Distributed under the terms of the MIT license.
#
import sys
import traceback
from datetime import date


import re
import requests
import json

import pywikibot
from pywikibot import pagegenerators
from datetime import datetime, timedelta
from pywikibot.bot import (
    ExistingPageBot, NoRedirectPageBot)
import pywikibot.exceptions
import pywikibot.data.sparql
import mwparserfromhell

from authorityFilter import authorityFilter

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}  # noqa: N816


class boxMiner(
    ExistingPageBot
):
    def __init__(self, generator, runstarttime, **kwargs):
        self.available_options.update({
            'createtaxo': False
        })

        super(boxMiner, self).__init__(**kwargs)

        self.createtaxo = kwargs['createtaxo']
        self.authorityFilter = authorityFilter()
        self.generator = generator
        self.datasite = pywikibot.Site('wikidata', 'wikidata')
        self.runstarttime = runstarttime
        self.removeChars = re.compile(r"['[\]]+")
        self.quickout = ""
        self.rundate = date.today().isoformat()
        with open("quickfile.txt", "w") as quickfile:
            quickfile.write('')

        self.humans = {}

    def write_output_line(self, line):
        with open("quickfile.txt", "a") as quickfile:
            quickfile.write(line)

    def teardown(self):
        pass

    def treat_page(self):
        summary = ""
        monotypic = False
        page_t = self.current_page.title()
        bi = page_t.find('(')
        page_t_trim = page_t if bi == -1 else page_t[:bi].strip()
        age = self.runstarttime - self.current_page.editTime()
        pywikibot.output('{} is {} old'.format(page_t, str(age)))
        taxonbar = None
        autoadded = ''
        autosum = ''
        genusparam = None
        taxonparam = None
        speciesparam = None
        authparam = None
        genus = ''
        recomb = False
        mwtree = mwparserfromhell.parse(self.current_page.text)
        item_name = ''
        item_year = ''
        # pywikibot.output(mwtree.get_tree())

        try:
            p_item = pywikibot.ItemPage.fromPage(self.current_page)
            p_item_cont = None
            if not 'P846' in p_item.claims:
                pywikibot.output("No GBIF ID")
                return
            gbifId = p_item.claims.get('P846')[0].getTarget()
            if 'P225' in p_item.claims:
                name_item = p_item.claims.get('P225')[0]
                if 'P405' in name_item.qualifiers:
                    return
                item_name = str(name_item.getTarget())
                pywikibot.output('"' + item_name + '"')
                if 'P574' in name_item.qualifiers:
                    item_year = name_item.qualifiers.get('P574')[0].target
                    pywikibot.output(f"Item year = {item_year}")
            auth = []
            year = None
            # pywikibot.output(p_item.claims)
            for template in mwtree.filter_templates():
                if template.name.matches(["Taxobox", "Automatic taxobox", "Speciesbox"]):
                    if template.has("genus"):
                        genusparam = template.get("genus")
                        pywikibot.output(genusparam)
                    if template.has("species"):
                        speciesparam = template.get("species")
                        pywikibot.output(speciesparam)
                    if template.has("binomial"):
                        taxonparam = template.get("binomial")
                        pywikibot.output(taxonparam)
                    if template.has("taxon"):
                        taxonparam = template.get("taxon")
                        pywikibot.output(taxonparam)
                    if template.has("authority"):
                        authparam = template.get("authority")
                    if template.has("binomial_authority"):
                        authparam = template.get("binomial_authority")
                    if template.has("genus_authority"):
                        authparam = template.get("genus_authority")
            jResults=None
            if authparam:
                with requests.get(url=f"https://api.gbif.org/v1/species/{gbifId}") as resp:
                    pywikibot.output(resp.status_code)
                    if (resp.status_code != 200):
                        raise Exception("API responded " +
                                        str(resp.status_code))

                    jResults = json.loads(resp.content)
                    if not 'authorship' in jResults:
                        raise Exception("API response had no author")

                for node in authparam.value.filter(recursive=False):
                    pywikibot.output(str(node) + ' ' + str(type(node)))
                    if type(node) == mwparserfromhell.nodes.text.Text:
                        textVal = str(node.value).replace(',', '').strip()
                        if (textVal):
                            if textVal.strip() in [',', '&']:
                                pass
                            elif len(auth) == 0:
                                if textVal == '(':
                                    recomb = True
                                else:
                                    pywikibot.output(
                                        'unexpected text: ' + textVal)
                                    matches = re.finditer(
                                        r" +\[?\d\d\d\d\]?", textVal)
                                    if matches:
                                        for match in matches:

                                            year = match[0].strip()
                                            textVal = textVal[:match.start()]
                                            if textVal[:1] == '(':
                                                recomb = True
                                                textVal = textVal[1:].replace(
                                                    ')', '')
                                            pywikibot.output(
                                                f"year = {match[0]} textVal={textVal}")
                                            rsvQids=self.authorityFilter.getQIDs(textVal, jResults, year)
                                            if rsvQids:
                                                 auth.append(
                                                        {'text': textVal, 'qid':rsvQids[0]})
                            else:
                                year = textVal.replace('[', '').replace(
                                    ']', '').replace(')', '').strip()

                    elif type(node) == mwparserfromhell.nodes.wikilink.Wikilink:
                        if year:
                            pywikibot.output('unexpected link: ' + str(node))
                        auth.append({'title': str(node.title), 'text': str(
                            node.text if node.text else node.title)})
                    elif type(node) == mwparserfromhell.nodes.tag.Tag:
                        pass  # refs
                    else:
                        pywikibot.output('unexpected:' + str(node))

            searchval = None
            if taxonparam:
                searchval = str(taxonparam.value)
            else:
                if speciesparam and genusparam:
                    searchval = str(genusparam.value).replace('\n', '').strip(
                    ) + ' ' + str(speciesparam.value).replace('\n', '').strip()
                else:
                    if genusparam:
                        searchval = str(genusparam.value).strip()

            searchval = searchval.replace(
                "'", "").strip() if searchval else searchval
            if searchval != item_name:
                pywikibot.output(f"{searchval} <> {item_name}")
            # and not recomb and item_name == searchval
            if item_name and year and len(auth):
                searchval = self.removeChars.sub('', searchval.strip())

                page = None
                pywikibot.output(item_name + "=GBIF: " + gbifId)
                missing_auth = []
                pywikibot.output(resp.content)
                pywikibot.output(
                    jResults['rank'] + ' ' + jResults['canonicalName'])
                if jResults['canonicalName'] != item_name:
                    raise Exception(
                        jResults['canonicalName'] + "!=" + item_name)
                gbif_auth = jResults['authorship']
                quickline = f'{p_item.getID()}\tP225\t"{item_name}"'

                for aut in auth:
                    pywikibot.output(aut)

                    if "qid" not in aut and not aut['title'] in self.humans:
                        a_page = pywikibot.Page(
                            pywikibot.Site(), aut['title'])
                        if a_page.isRedirectPage():
                            a_page = a_page.getRedirectTarget()
                        a_item = pywikibot.ItemPage.fromPage(a_page)
                        pywikibot.output(a_item.getID())
                        if 'P31' in a_item.claims and a_item.claims.get('P31')[0].target_equals('Q5'):
                            self.humans[aut['title']] = a_item.getID()
                    if "qid" in aut or aut['title'] in self.humans:
                        gbif_auth_new = re.sub(
                            aut['text'], '', gbif_auth, flags=re.I)
                        if gbif_auth_new == gbif_auth:
                            missing_auth.append(aut)
                        gbif_auth = gbif_auth_new
                        quickline += f"\tP405\t{aut['qid'] if 'qid' in aut else self.humans[aut['title']]}"
                    else:
                        pywikibot.output('Authority must be a human being')
                        pywikibot.output(aut['title'])
                gbif_auth = gbif_auth.replace(
                    year, '').replace(',', '').strip()
                pywikibot.output(gbif_auth)
                pywikibot.output(year)
                if not item_year:
                    quickline += f'\tP574\t+{year}-00-00T00:00:00Z'

                attribution = f'\tS248\tQ1531570\tS813\t+{self.rundate}T00:00:00Z/11'
                gbif_auth = gbif_auth.replace('&', '').replace(' ', '')
                gbif_auth = gbif_auth.strip()
                recomb_comment = ''
                if recomb:
                    if gbif_auth == '()':
                        gbif_auth = ''
                    else:
                        if 'issues' in jResults and "PUBLISHED_BEFORE_GENUS" in jResults["issues"]:
                            gbif_auth = ''
                            attribution = f'\tS143\tQ328 /* recombination shown on enwiki, GBIF flags PUBLISHED_BEFORE_GENUS issue */'
                        else:
                            gbif_auth = 'recomb disparity'
                elif gbif_auth == '()':
                    gbif_auth = 'recomb disparity - not on enwiki'
                if recomb:
                    quickline += f'\tP3831\tQ14594740'
                quickline += attribution + '\n'
                if not gbif_auth and not missing_auth:
                    self.write_output_line(quickline)
                else:
                    pywikibot.output(
                        'Disparity: ' + gbif_auth + ' ' + str(missing_auth))
        except Exception as e:
            traceback.print_exc()


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    options = {'createtaxo': False}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators.GeneratorFactory()

    # Parse command line arguments
    for arg in local_args:

        # Now pick up your own options
        option, sep, value = arg.partition(':')
        if option == "-agelimit":
            options['agelimit'] = int(value)
            continue
        if option == "-createtaxo":
            options['createtaxo'] = True
            continue
        # Catch the pagegenerators options
        if gen_factory.handle_arg(arg):
            continue  # nothing to do here

    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        bot = boxMiner(gen, gen_factory.site.server_time(), **options)
        bot.run()
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
