from zeep import Client

client = Client('https://www.itis.gov/ITISWebService/services/ITISService?wsdl')
service=client.create_service(
    "{http://itis_service.itis.usgs.gov}ITISServiceSoap12Binding",
    "https://www.itis.gov/ITISWebService/services/ITISService.ITISServiceHttpsSoap12Endpoint/")
#rec=service.getFullRecordFromTSN(tsn=1102413)
#rec=service.getFullRecordFromTSN(tsn=1099916)
#print(rec)
#exit()

rec=service.getSynonymNamesFromTSN(tsn=775918)

print(type(rec))
print(rec)
for synonym in rec.synonyms:
    md=service.getCoreMetadataFromTSN(tsn=synonym.tsn)
    print(md)

