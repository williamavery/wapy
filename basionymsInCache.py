from datetime import date

import pywikibot, pywikibot.data.sparql as sparql
from pywikibot.page import WikibasePage

from cacher import cache as GBIF_cache

import warnings
warnings.simplefilter("ignore")

quickfilename="basio_quickfile.txt"
site=pywikibot.Site('wikidata', 'wikidata')
rundate = date.today().isoformat()
def runqry(gbifid):
    squery= f"""SELECT ?item 
    WHERE
    {{
        ?item p:P846 ?statement0.
        ?statement0 (ps:P846) "{gbifid}".
    }}"""
    qry= pywikibot.data.sparql.SparqlQuery().select(query=squery)
    if not len(qry):
        return None
    it = str(qry[0]['item'])
    s = it.split('/')
    s.reverse()
    qid = s[0] if s else None
    return qid

cache = GBIF_cache()

with open(quickfilename, "w") as quickfile:
    quickfile.write('')
#target='Say,'
for item in cache.filter(lambda item: 
                        'basionym' in item[1] #and
                        #item[1]['authorship'].strip()[1:len(target) + 1] == target
                        ):
    #print(item[1])

    authorship=item[1]['authorship'].strip()[1:-1]
    qid = runqry(item[1]['basionymKey'])
    recombqid = runqry(item[0])

    if qid and recombqid:
            print(f"{item[1]['basionym']} {item[1]['canonicalName']}")
            generate_protonym=True
            generate_oc=True
            pg=pywikibot.ItemPage(site, qid)
            if 'P225' not in pg.claims:
                pywikibot.output(f'No taxon name on {qid}')
                continue
            names = pg.claims.get('P225')
            if len(names) != 1:
                pywikibot.output(f'Expected one taxon name on {qid}, found {len(names)}')
                continue
            tname=names[0].getTarget()
            if not item[1]['basionym'].startswith(tname + ' ' + authorship):
                pywikibot.output(f"Expected match for {item[1]['basionym']} on {qid}, found {tname}")
                continue
            recombpg=pywikibot.ItemPage(site, recombqid)
            print(pg)
            if 'P2868' in pg.claims:
                for protonym in [r for r in pg.claims.get('P2868') if r.target_equals('Q14192851')]:
                    if protonym.has_qualifier('P642', recombqid):
                        generate_protonym=False
                        continue
            print(f'generate_protonym={generate_protonym}')
            if generate_protonym:
                quickline = f'{qid}\tP2868\tQ14192851\tP642\t{recombqid}'
                quickline += f'\tS248\tQ1531570\tS813\t+{rundate}T00:00:00Z/11\n'
                with open(quickfilename, "a") as quickfile:
                    quickfile.write(quickline)
            if 'P1403' in recombpg.claims:
                if [r for r in recombpg.claims.get('P1403') if r.target_equals(qid)]:
                        generate_oc=False
                        continue
            print(f'generate_oc={generate_oc}')
            if generate_oc:
                quickline = f'{recombqid}\tP1403\t{qid}'
                quickline += f'\tS248\tQ1531570\tS813\t+{rundate}T00:00:00Z/11\n'
                with open(quickfilename, "a") as quickfile:
                    quickfile.write(quickline)