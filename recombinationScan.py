#!/usr/bin/python
"""
Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following generators and filters are supported but
cannot be set by settings file:

&params;
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2021
# (C) William Avery, 2021
#
# Distributed under the terms of the MIT license.
#

import sys
import os
from datetime import date
from time import sleep
import warnings
import requests
import json
import zeep
from zeep import Client, Transport

import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import (
    ExistingPageBot,
    SingleSiteBot
)

import sparqlfilehandler

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}


class recombinationScan(
    ExistingPageBot,
    SingleSiteBot
):

    @staticmethod
    def qids_with_propval(prop, id):
        return pywikibot.data.sparql.SparqlQuery().select(query=f"""
        SELECT (STRAFTER(STR(?item), STR(wd:)) as ?QID)
        WHERE
        {{
            ?item p:{prop} ?statement0.
            ?statement0 ps:{prop} "{id}".
        }}""")

    @staticmethod
    def qid_with_propval(prop, id):
        if qry_reslt := recombinationScan.qids_with_propval(prop, id):
            return qry_reslt[0]['QID']
        else:
            return None

    @staticmethod
    def common_parent_at_rank(rank, taxon1, taxon2):
        reslt=pywikibot.data.sparql.SparqlQuery().select(query=f"""
            SELECT DISTINCT ("Y" as ?tst)
            WHERE
            {{
            wd:{taxon1} wdt:P171+ ?ancestors.
            wd:{taxon2} wdt:P171+ ?ancestors.
            ?ancestors p:P105 ?rank.
            ?rank ps:P105 wd:{rank}
            }}""")
        return len(reslt) if reslt else 0

    def __init__(self, generator, **kwargs):
        self.available_options.update({
            'sparqlfile': None,
            'SPARQLPARAM': None,
            'outputfile': None
        })

        super(recombinationScan, self).__init__(**kwargs)

        self.generator = generator

        self.quickfilename = kwargs['outputfile']
        with open(self.quickfilename, "w") as quickfile:
            quickfile.write('')
        itis_WSDLurl  ='https://www.itis.gov/ITISWebService/services/ITISService?wsdl'
        itis_localWSDL=os.path.expanduser("~") + '/ITISService.xml'
        if os.path.isfile(itis_localWSDL) :
            itis_WSDLurl  ='//' + itis_localWSDL 
        itis = Client(itis_WSDLurl,
                      transport=Transport(timeout=60, operation_timeout=60))
        self.ITIS = itis.create_service(
            "{http://itis_service.itis.usgs.gov}ITISServiceSoap12Binding",
            "https://www.itis.gov/ITISWebService/services/ITISService.ITISServiceHttpsSoap12Endpoint/")
        warnings.filterwarnings('once', category=ResourceWarning)
        worms = Client('https://www.marinespecies.org/aphia.php?p=soap&wsdl=1')
        self.WORMS = worms.service

        self.missingparents=[]

    def teardown(self):
        pywikibot.output(f"{self.missingparents=}")

    def write_output_line(self, line):
        with open(self.quickfilename, "a") as quickfile:
            quickfile.write(line)

    def queryITIS(self, func, **kwargs):
        response = None
        retry_limit = 100
        retries = 0
        while retries < retry_limit:
            try:
                response = self.ITIS[func](**kwargs)
                break
            except requests.exceptions.RequestException as exc:
                pywikibot.output(repr(exc))
                retries += 1
                interval = min([retries * 2, 60])
                pywikibot.output(
                    f"Retrying ITIS in {interval}s. Retry {retries}")
                sleep(interval)
            except zeep.exceptions.Fault as exc:
                pywikibot.output(repr(exc))
                break
        return response

    def queryWORMS(self, func, **kwargs):
        args = []
        response = None
        retry_limit = 100
        retries = 0
        while retries < retry_limit:
            try:
                response = self.WORMS[func](**kwargs)
                break
            except (requests.exceptions.RequestException) as exc:
                pywikibot.output(repr(exc))
                retries += 1
                interval = min([retries * 2, 60])
                pywikibot.output(
                    f"Retrying WORMS in {interval}s. Retry {retries}")
                sleep(interval)
        return response

    def queryGBIF(self, gbifId):
        response = None
        retry_limit = 20
        retries = 0
        while retries < retry_limit:
            try:
                with requests.get(url=f"https://api.gbif.org/v1/species/{gbifId}") as resp:
                    if (resp.status_code != 200):
                        pywikibot.output(
                            f"GBIF API responded {resp.status_code} on {gbifId}")
                        if resp.status_code not in [408, 409, 429]:
                            break
                    else:
                        response = json.loads(resp.content)
                        break
            except (requests.exceptions.RequestException) as exc:
                pywikibot.output(repr(exc))

            retries += 1
            interval = min([retries * 2, 60])
            pywikibot.output(f"Retrying GBIF in {interval}s. Retry {retries}")
            sleep(interval)
        return response

    def getIdentifier(self, prop):
        if not prop in self.current_page.claims:
            pywikibot.output(f"No {prop}")
            return
        val = self.current_page.claims.get(prop)[0].getTarget()
        pywikibot.output(f"{self.item_name} has {prop}={val}")
        return val

    def parent_from_scientific_name(self, ocScientificName, searchname, qid):
        parentqid=None
        if (ocScientificName not in self.createList 
          and searchname not in self.missingparents):
            parentqids = self.qids_with_propval('P225', searchname)
            for rank in ['Q35409', 'Q36602', 'Q37517', 'Q38348']:
                for candidate in parentqids:
                    if self.common_parent_at_rank(rank, qid, candidate['QID']):
                        parentqid=candidate['QID']
                        pywikibot.output(f"found OC parent QID {parentqid} at {rank} using {searchname}")
                        break                    
                if parentqid :
                    break
            if not parentqid:
                if searchname not in self.missingparents: self.missingparents.append(searchname)
        return parentqid

    def try_itis(self):
        prop = 'P815'
        if not (itisTSN := self.getIdentifier(prop)):
            return

        if not (synonymNames := self.queryITIS('getSynonymNamesFromTSN', tsn=itisTSN)):
            pywikibot.output("getSynonymNames response was falsy")
            return
        oc=None
        qid=self.current_page.getID()
        for synonym in synonymNames.synonyms :
            if not (coreMetadata := self.queryITIS('getCoreMetadataFromTSN', tsn=synonym.tsn)):
                pywikibot.output("coreMetadata response was falsy")
                continue
            if coreMetadata.unacceptReason=='original name/combination':
                oc=coreMetadata
                break

        if not oc:
            pywikibot.output("No OC found on ITIS")
            return

        if not (scientificName := self.queryITIS('getScientificNameFromTSN', tsn=itisTSN)):
            pywikibot.output("scientificName response was falsy")
            return

        if scientificName.combinedName != self.item_name:
            pywikibot.output(f"scientificName {scientificName.combinedName} mismatch with {self.item_name}")
            return
        if not (ocScientificName := self.queryITIS('getScientificNameFromTSN', tsn=oc.tsn)):
            pywikibot.output("scientificName response for OC was falsy")
            return
        if not (ocqid := self.qid_with_propval(prop, oc.tsn)): 
            pywikibot.output(f"failed to find QID for {prop}={oc.tsn}")
            if not (ocqid := self.qid_with_propval('P225', ocScientificName.combinedName)): 
                pywikibot.output(f"failed to find QID for {ocScientificName.combinedName}")
        if not ocqid:
            searchname=ocScientificName.unitName1
            if (ocScientificName.unitName3):
                searchname += " " + ocScientificName.unitName2
            # get parent tsn
            parentqid = self.parent_from_scientific_name(ocScientificName.combinedName, searchname, qid)
            #    pywikibot.output(f"failed to find parent QIDTrito
            if parentqid:
                self.createList.append(ocScientificName.combinedName)
                self.ocdets.update({
                        'parentqid': parentqid,
                        'rank': 'Q68947' if ocScientificName.unitName3 else 'Q7432',
                        'name': ocScientificName.combinedName,
                        'tsn': oc.tsn,
                        'org': 'Q82575',
                    })

        pywikibot.output(f'{ocqid=}')
        if ocqid: 
            self.ocpg=pywikibot.ItemPage(self.site, ocqid)
            self.org = 'Q82575'
            return True

    def try_worms(self):
        prop = 'P850'
        if not (aphiaID := self.getIdentifier(prop)):
            return
        oc=None
        qid=self.current_page.getID()
        offset=1
        while offset and not oc:        
            if len(synonyms := self.queryWORMS('getAphiaSynonymsByID', AphiaID=aphiaID, offset=offset)) < 50:
                offset=None
            else:
                offset+=50
            for aphiaRec in synonyms :
                if not aphiaRec.AphiaID:
                    offset = False
                    break
                if aphiaRec.unacceptreason=='original combination':
                    pywikibot.output('WoRMS protonym')
                    oc=aphiaRec
                    break
        if not oc:
            pywikibot.output("No OC found on WORMS")
            return

        if not (ocqid := self.qid_with_propval(prop, oc.AphiaID)): 
            pywikibot.output(f"failed to find QID for {prop}={oc.AphiaID}")
            if not (ocqid := self.qid_with_propval('P225', oc.scientificname)): 
                pywikibot.output(f"failed to find QID for {oc.scientificname}")

        if not (recomb := self.queryWORMS('getAphiaRecordByID', AphiaID=aphiaID)):
            pywikibot.output(f"Aphia response was falsy for {aphiaID=}")
            return

        if recomb.scientificname != self.item_name:
            pywikibot.output(f"scientificname {recomb.scientificname} mismatch with {self.item_name}")
            return

        pywikibot.output(f'{ocqid=}')
        if ocqid: 
            self.ocpg=pywikibot.ItemPage(self.site, ocqid)
            self.org='Q604063'
            return True

        # get parent aphiaID
        parentname=self.parentname_from_name(oc.scientificname)
        parentqid = self.parent_from_scientific_name(
            oc.scientificname,                     
            parentname, # 
            qid)
        if parentqid:
            self.createList.append(oc.scientificname)
            self.ocdets.update({
                'parentqid': parentqid,
                'rank': 'Q68947' if oc.rank=='Subspecies' else 'Q7432',
                'name': oc.scientificname,
                'aphiaID': oc.AphiaID,
                'org': 'Q604063',
            })

    def parentname_from_name(self, name):
        parentarr=name.split(' ')
        parentarr.pop()
        return ' '.join(parentarr)

    def try_gbif(self):
        prop='P846'
        if not (gbifID := self.getIdentifier(prop)):
            return
        oc=None
        qid=self.current_page.getID()

        if not (recomb := self.queryGBIF(gbifID)):
            pywikibot.output(f"GBIF rec for {gbifID} was falsy")
            return
        if 'basionymKey' in recomb and recomb['basionymKey']!=int(gbifID):
            pywikibot.output('GBIF "basionym"')
        else:
            pywikibot.output("No OC found on GBIF")
            return

        oc = self.queryGBIF(recomb['basionymKey'])
        if not oc:
            pywikibot.output(f"GBIF rec for {recomb['basionymKey']} was falsy")
            return

        if not (ocqid := self.qid_with_propval(prop, recomb['basionymKey'])): 
            pywikibot.output(f"failed to find QID for {prop}={recomb['basionymKey']}")
            if not (ocqid := self.qid_with_propval('P225', oc['canonicalName'])): 
                pywikibot.output(f"failed to find QID for {oc['canonicalName']}")

        if recomb['canonicalName'] != self.item_name:
            pywikibot.output(f"GBIF: {recomb['canonicalName']} mismatch with {self.item_name}")
            return

        pywikibot.output(f'{ocqid=}')
        if ocqid: 
            self.ocpg=pywikibot.ItemPage(self.site, ocqid)
            self.org='Q1531570'
            return True

        # get parent gbifID
        parentname=self.parentname_from_name(oc['canonicalName'])
        parentqid = self.parent_from_scientific_name(
                    oc['canonicalName'],
                    parentname, # 
                    qid)
        if parentqid:
            self.createList.append(oc['canonicalName'])
            self.ocdets.update({
                'parentqid': parentqid,
                'rank': 'Q68947' if oc['rank']=='SUBSPECIES' else 'Q7432',
                'name': oc['canonicalName'],
                'gbifID': oc['key'],
                'org': 'Q1531570',
            })

    def process_oc(self):
        p_item = self.current_page
        qid = p_item.getID()
        ocqid=self.ocpg.getID()
        ref = f'\tS248\t{self.org}\tS813\t+{date.today().isoformat()}T00:00:00Z/11\n'
        
        generate_protonym = True
        generate_oc = True
        
        if 'P2868' in self.ocpg.claims:
            for protonym in [r for r in self.ocpg.claims.get('P2868') if r.target_equals('Q14192851')]:
                if protonym.has_qualifier('P642', qid):
                    generate_protonym = False
                    continue
        
        if 'P1403' in p_item.claims:
            if [r for r in p_item.claims.get('P1403') if r.target_equals(ocqid)]:
                generate_oc = False
        
        if generate_oc and 'P1403' in  p_item.claims:   # A name should only have one OC
            pywikibot.output(f'recombination {qid}, already has OC')
            return
        
        if generate_protonym:
            self.write_output_line(
                f'{ocqid}\tP2868\tQ14192851\tP642\t{qid}{ref}')
        
        if generate_oc:
            self.write_output_line(f'{qid}\tP1403\t{ocqid}{ref}')

    def treat_page(self) -> None:
        self.ocpg = None
        self.ocdets = {}
        self.org = None
        self.createList=[]

        p_item = self.current_page
        qid = p_item.getID()
        self.item_name = ''

        if 'P225' in p_item.claims:
            names = p_item.claims.get('P225')
            if len(names) != 1:
                pywikibot.output(
                    f'Expected one taxon name on {qid}, found {len(names)}')
                return
            self.item_name = str(names[0].getTarget())

        self.try_itis() or self.try_worms() or self.try_gbif()

        if (self.ocpg):
            self.process_oc()
            return

        if self.ocdets:
            iso_today=f"+{date.today().isoformat()}T00:00:00Z/11"
            ref = f"\tS248\t{self.ocdets['org']}\tS813\t{iso_today}\n"
            ranklabel = 'Subspecies' if self.ocdets['rank']=='Q68947' else 'Species'
            self.write_output_line("CREATE\n")
            self.write_output_line(f"LAST\tDen\t\"{ranklabel} of commonnameplaceholder\"\n")
            self.write_output_line(f"LAST\tDnl\t\"taxon\"\n")
            for lang in ['en', 'es', 'fi', 'fr', 'pl', 'pt', 'de', 'ca', 'it' , 'nl', 'la', 'sq', 'vi',
                            'ast', 'ga', 'ro', 'bg', 'ru', 'war', 'ceb', 'an', 'eo', 'eu', 'ext', 'gl', 'ia', 'ie', 'io', 'oc', 'pt-br', 'vo']:
                self.write_output_line(f"LAST\tL{lang}\t\"{self.ocdets['name']}\"\n")
            self.write_output_line(f"LAST\tP31\tQ16521{ref}")
            self.write_output_line(f"LAST\tP225\t\"{self.ocdets['name']}\"{ref}")
            self.write_output_line(f"LAST\tP105\t{self.ocdets['rank']}{ref}")
            self.write_output_line(f"LAST\tP171\t{self.ocdets['parentqid']}\n")
            self.write_output_line(f'LAST\tP2868\tQ14192851\tP642\t{qid}{ref}')
            if 'tsn' in self.ocdets:
                self.write_output_line(f"LAST\tP815\t\"{self.ocdets['tsn']}\"\t" +
                    f"S248\tQ82575\tS813\t{iso_today}\n")
            if 'aphiaID' in self.ocdets:                
                self.write_output_line(f"LAST\tP850\t\"{self.ocdets['aphiaID']}\"\t" +
                    f"S248\tQ604063\tS813\t{iso_today}\n")
            if 'gbifID' in self.ocdets:
                self.write_output_line(f"LAST\tP846\t\"{self.ocdets['gbifID']}\"\t" +
                    f"S248\tQ1531570\tS813\t{iso_today}\n")



def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {'outputfile': f'./quickstats_{os.getpid()}.txt'}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine on which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # generate -sparql parameter from sparqlfilehandler parameters
    local_args = sparqlfilehandler.handle_params(local_args, echo=True)

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(':')
        option = arg[1:]
        if option in ('outputfile'):
            if not value:
                pywikibot.input('Please enter a value for ' + arg)
            options[option] = value
        # take the remaining options as booleans.
        else:
            options[option] = True
    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        # pass generator and private options to the bot
        bot = recombinationScan(generator=gen, **options)
        bot.run()  # guess what it does
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
