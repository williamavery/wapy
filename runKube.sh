export QID=$1
export TOOL=william-avery-bot
export TOOLHOME=/data/project/$TOOL
export NAME=protonymscan-${QID,,}
ssh williamavery@login.toolforge.org << EOF
become $TOOL
kubectl create --validate=true -f -  <<END
 apiVersion: batch/v1
 kind: Job
 metadata:
   name: $NAME
   labels:
     name: $NAME
     toolforge: $TOOL
 spec:
   template:
     metadata:
       labels:
         name: $NAME
         toolforge: tool # Required. Causes the shares to be mounted.
     spec:
       containers:
         - name: $NAME
           image: docker-registry.tools.wmflabs.org/toolforge-python37-sssd-base:latest
           command: [ "python3",
                      "$TOOLHOME/wikipythonics/protonymScan.py",
                      "-family:wikidata", "-lang:wikidata",
                      "-sparqlfile:$TOOLHOME/wikipythonics/refhunt.sparql",
                      "-SPARQLPARAM:$QID" ]
           workingDir: $TOOLHOME
           env:
             - name: PYTHONPATH
               value: /data/project/shared/pywikibot/stable:$TOOLHOME/www/python/venv/lib/python3.7/site-packages
             - name: HOME
               value: $TOOLHOME
           imagePullPolicy: Always
       restartPolicy: Never
   backoffLimit: 0 # i.e. never really means never
END
EOF
