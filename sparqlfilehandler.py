#
# (C) William Avery, 2021
#
# Distributed under the terms of the MIT license.
#
class sparqlfilehandlerException(Exception):
    def __init__(self, errorArguments):
       super().__init__(f"Error in SPARQL file handler: {errorArguments}")

def handle_params(local_args, echo=False):
    # Construct a SPARQL query from sparqlfile and SPARQLPARAM if supplied
    options={}
    removeargs=[]
    for argval in local_args:
        arg, sep, value = argval.partition(':')
        option = arg[1:]
        if option in ('sparqlfile', 'SPARQLPARAM'):
            if not value:
                raise sparqlfilehandlerException(arg + ' option specified without value')
            options[option] = value
            removeargs.append(argval)
    if ('sparqlfile' in options):
        with open(options['sparqlfile'], 'r') as sfile:
            tempqry=sfile.read()
            if ('SPARQLPARAM' in options):
                tempqry = tempqry.replace('$SPARQLPARAM', options['SPARQLPARAM'])
            if tempqry:    
                local_args.append('-sparql:' + tempqry)
                if echo: print(tempqry)
    else:
        if ('SPARQLPARAM' in options):
            raise sparqlfilehandlerException('SPARQLPARAM parameter cannot be used without sparqlfile')
    for arg in removeargs: local_args.remove(arg)
    return local_args