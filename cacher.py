import os
import pickle
import json

class cache(dict):
    def __init__(self, filename="~/.GBIF_cache.txt"):
        self.cacheFile = os.path.expanduser(filename)
        with open(self.cacheFile, 'rb') as cf:
            self._cache =  pickle.load(cf)
        super(dict, self._cache)

    @property
    def items(self):
        for k, v  in self._cache.items():
            yield (k, json.loads(v))

    def filter(self, function):
        return filter(function, self.items)

